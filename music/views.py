from django.views import generic
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from .models import Albums

class IndexView(generic.ListView):
    template_name = 'music/index.html'
    context_object_name = 'all_albums'
    def get_queryset(self):
        return Albums.objects.all()

class DetailView(generic.DetailView):
    model = Albums
    template_name = 'music/detail.html'

class AlbumsCreate(CreateView):
    model = Albums
    filter = ['artist', 'album_title', 'genre', 'album_logo']
